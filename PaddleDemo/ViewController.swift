//
//  ViewController.swift
//  PaddleDemo
//
//  Created by 曾嘉伦 on 2021/12/6.
//

import UIKit

let screenHeight: CGFloat = UIScreen.main.bounds.height
let screenWidth: CGFloat = UIScreen.main.bounds.width

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        view.backgroundColor = .white
        let list = ["paddle分割"]
        let btnWidth = 150.0
        let btnHeight = 40.0
        for (index, element) in list.enumerated() {
            let enterBtn = UIButton(type: .custom)
            enterBtn.frame = CGRect(x: (screenWidth - btnWidth) / 2.0, y: CGFloat(150 + index * (30 + Int(btnHeight))), width: btnWidth, height: btnHeight)
            enterBtn.backgroundColor = .red
            enterBtn.setTitle(element, for: .normal)
            enterBtn.addTarget(self, action: #selector(enterBtnClick(sender:)), for: .touchUpInside)
            enterBtn.tag = index + 1
            self.view.addSubview(enterBtn)
        }
    }

    @objc func enterBtnClick(sender: UIButton) {
        let tag = sender.tag
        if tag == 1 {
            let vc = PaddleViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

