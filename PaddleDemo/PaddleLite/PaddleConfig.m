//
//  PaddleConfig.m
//  PaddleDemo
//
//  Created by 曾嘉伦 on 2021/12/8.
//

#import "PaddleConfig.h"

@interface PaddleConfig ()

@property (nonatomic, assign) ModelType modelType;// 模型类型
@property (nonatomic, strong) NSString * modelPath;// 模型地址
@property (nonatomic, strong) NSString * modelName;
@property (nonatomic, strong) NSString * metalPath;// metal库地址
@property (nonatomic, assign) PaddleShape inputShape;// 输入图片 tensor格式
@property (nonatomic, assign) PaddleShape coordShape;// 输入点 tensor格式（与inputShape一致）
@property (nonatomic, assign) int minShapeWidth;

@end

@implementation PaddleConfig

- (instancetype)init{
    self = [super init];
    if (self) {
        NSString *path = [[NSBundle mainBundle] bundlePath];
        self.modelType = LOW_COMMON_METAL;
        self.cpuThreadNum = 1;
        self.cpuPowerMode = LITE_POWER_NO_BIND;
        self.threshold = 0.5;
        self.minShapeWidth = 300;
        self.metalPath = [NSString stringWithFormat:@"%@%@",path, @"/lite.metallib"];
        [self updateInputShapeWidth: self.minShapeWidth height:self.minShapeWidth];
        [self updateModelWithType: self.modelType];
    }
    return self;
}

- (void)updateInputShapeWidth:(int)width height:(int)height{
//    CGFloat whScale = width * 1.0 / height;
//
//    if (width <= height) {
//        width = MIN(width, self.minShapeWidth);
//        height = width / whScale;
//    }else{
//        height = MIN(height, self.minShapeWidth);
//        width = height * whScale;
//    }
    
    PaddleShape shape = {1, 3, height, width};
    self.inputShape = shape;
    self.inputShape = shape;
    self.coordShape = shape;
    self.coordShape = shape;
}

- (void)updateModelWithType:(ModelType)type {
    self.modelPath = [self getModelPathWithType: type];
    self.modelName = [self getModelNameWithType: type];
}

- (NSString *)getModelNameWithType:(ModelType)type {
    switch (type) {
        case HIGH_COMMON:
            return @"通用高精度";
            break;
        case LOW_COMMON:
            return @"通用轻量化";
            break;
        case LOW_COMMON_METAL:
            return @"GPU通用轻量化";
            break;
        case HIGH_FACE:
            return @"人像高精度";
            break;
        case LOW_FACE:
            return @"人像轻量化";
            break;
            
        default:
            break;
    }
}

- (NSString *)getModelPathWithType:(ModelType)type {
    NSString *path = [[NSBundle mainBundle] bundlePath];
    
    switch (type) {
        case HIGH_COMMON:
            return [NSString stringWithFormat:@"%@%@",path, @"/model_seg_common_full.nb"];
            break;
        case LOW_COMMON:
            return [NSString stringWithFormat:@"%@%@",path, @"/model_seg_common_lite.nb"];
            break;
        case LOW_COMMON_METAL:
            return [NSString stringWithFormat:@"%@%@",path, @"/model_seg_common_lite_metal.nb"];
            break;
        case HIGH_FACE:
            return [NSString stringWithFormat:@"%@%@",path, @"/model_seg_face_full.nb"];
            break;
        case LOW_FACE:
            return [NSString stringWithFormat:@"%@%@",path, @"/model_seg_face_lite.nb"];
            break;
            
        default:
            break;
    }
}

- (id)copyWithZone:(NSZone *)zone {
    
    PaddleConfig * config = [[PaddleConfig alloc] init];
    
    config.threshold = self.threshold;
    config.modelType = self.modelType;
    config.modelPath = self.modelPath;
    config.modelName = self.modelName;
    config.inputShape = self.inputShape;
    config.coordShape = self.coordShape;
    config.cpuThreadNum = self.cpuThreadNum;
    config.cpuPowerMode = self.cpuPowerMode;
    
    return config;
}



@end
