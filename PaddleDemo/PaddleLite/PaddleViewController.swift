//
//  PaddleViewController.swift
//  PaddleDemo
//
//  Created by 曾嘉伦 on 2021/12/6.
//

import UIKit
import opencv2

class PaddleViewController: UIViewController {
    
    let imageOffsetY: CGFloat = 10.0 + 88.0
    var currentPoint: CGPoint = CGPoint.zero
    // 切割类
    let paddlePredictor = PaddlePredictorManager()
    var imageView = UIImageView()
    var image = UIImage(named: "person.png")
    let controlView = UIView()
    let modelBtn = UIButton(type: .custom)
    
    var shapeView: SliderView?
    var thresholdView: SliderView?
    // 显示耗时
//    let runTimeLabel = UILabel()
//    let processTimeLabel = UILabel()
//    let processTimeLabel = UILabel()
    var timeLabelList: [UILabel] = []
    
    // 系统相册
    lazy var photosPicker: UIImagePickerController = {
        let photosPicker = UIImagePickerController()
        photosPicker.delegate = self
        photosPicker.allowsEditing = true
        photosPicker.navigationBar.barTintColor = UIColor.green
        photosPicker.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.red]
        photosPicker.navigationBar.tintColor = UIColor.blue
        return photosPicker
    }()
    
    lazy var maskImageView: UIImageView = {
        let maskImageView = UIImageView()
        maskImageView.backgroundColor = UIColor.clear
        maskImageView.layer.magnificationFilter = .nearest
        return maskImageView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        resetImage()
        setNav()
        
        self.view.backgroundColor = .white
        imageView.contentMode = .center
        
        imageView.addSubview(maskImageView)
        self.view.addSubview(imageView)
        self.view.addSubview(controlView)
                
//        var lastOffset = 20.0
        var lastView:UIView?
        // 耗时显示label
        let timeArr = ["预处理耗时：", "模型推理耗时：", "输出处理耗时："]
        for _ in timeArr {
            let y = lastView?.frame.maxY ?? 20
            let timeLabel = UILabel(frame: CGRect(x: 20, y: y, width: screenWidth - 40, height: 15))
            timeLabel.textColor = .red
            timeLabel.font = UIFont.systemFont(ofSize: 13)
            controlView.addSubview(timeLabel)
            timeLabelList.append(timeLabel)
            lastView = timeLabel
        }
        updateTimeConsume()
        
        /// 模型切换
        let modelName = paddlePredictor.config.modelName
        modelBtn.frame = CGRect(x: 20, y: (lastView?.frame.maxY ?? 0) + 10, width: 200, height: 30)
        modelBtn.contentHorizontalAlignment = .left
        modelBtn.setTitle(String(format: " 当前模型：%@", modelName), for: .normal)
        modelBtn.setTitleColor(.black, for: .normal)
        modelBtn.setImage(UIImage(named: "setting_icon_s"), for: .normal)
        modelBtn.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        modelBtn.addTarget(self, action: #selector(modelSelectClick), for: .touchUpInside)
        controlView.addSubview(modelBtn)
        lastView = modelBtn
        
        // shape值滑块
        let shapeView = SliderView(frame: CGRect(x: 20, y: (lastView?.frame.maxY ?? 0) + 10, width: screenWidth - 40, height: 50))
        shapeView.slider.addTarget(self, action: #selector(shapeSliderChange(_:)), for: .touchUpInside)
        shapeView.updateShapeSliderMaxMinValueWithImage(image)
        controlView.addSubview(shapeView)
        self.shapeView = shapeView
        shapeSliderChange(shapeView.slider)
        lastView = shapeView
        // 阈值滑块
        let thresholdView = SliderView(frame: CGRect(x: 20, y: (lastView?.frame.maxY ?? 0) + 10, width: screenWidth - 40, height: 50))
        thresholdView.slider.addTarget(self, action: #selector(thresholdSliderChange(_:)), for: .valueChanged)
        thresholdView.slider.minimumValue = 0
        thresholdView.slider.maximumValue = 1
        thresholdView.minLabel.text = "0.0"
        thresholdView.maxLabel.text = "1.0"
        thresholdView.updateThresholdValue(0.5)
        controlView.addSubview(thresholdView)
        self.thresholdView = thresholdView
        thresholdSliderChange(thresholdView.slider)
        lastView = thresholdView
        
        let cpuNumView = SliderView(frame: CGRect(x: 20, y: (lastView?.frame.maxY ?? 0) + 10, width: screenWidth - 40, height: 50))
        cpuNumView.slider.addTarget(self, action: #selector(cpuNumSliderChange(_:)), for: .valueChanged)
        cpuNumView.slider.minimumValue = 1
        cpuNumView.slider.maximumValue = 1
        cpuNumView.minLabel.text = "1.0"
        cpuNumView.maxLabel.text = "1.0"
        cpuNumView.titleLabel.text = "cpu核心数：1"
        controlView.addSubview(cpuNumView)
        lastView = cpuNumView
        
//        image_preprocess(<#T##cv#>, <#T##paddle#>, <#T##Int32#>, <#T##Int32#>, <#T##std#>, <#T##std#>, <#T##Bool#>)
    }
    
    func setNav() {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        btn.setTitle("打开相册", for: .normal)
        btn.setTitleColor(.red, for: .normal)
        btn.addTarget(self, action: #selector(enterBtnClick(sender:)), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: btn)
        self.navigationItem.rightBarButtonItems = [barButton]
    }
    
    /// 运行预测
    func runModel() {
        guard currentPoint != CGPoint.zero else { return }
        // 进行图像分割
        paddlePredictor.runModel(image, point: currentPoint) { [weak self] in
            guard let self = self else { return }
            let threshold = self.paddlePredictor.config.threshold
            self.paddlePredictor.fetchPredictImage(byThreshold: threshold) { outputImage in
                self.maskImageView.image = outputImage
            }
            self.updateTimeConsume()
        }
    }
}

extension PaddleViewController {
    
    // 重置图片 并布局
    func resetImage() {
        imageView.transform = .identity
                
        let imageWidth: CGFloat = image?.size.width ?? 1
        let imageHeight: CGFloat = image?.size.height ?? 1
        
        var scale = 1.0
        if imageWidth > imageHeight{
            scale = (screenWidth - 40) / imageWidth
        } else {
            scale = (screenWidth - 40) / imageHeight
        }
                
        imageView.image = image
        imageView.frame = CGRect(x: 20, y: imageOffsetY, width: imageWidth, height: imageHeight)
        imageView.layer.position = CGPoint(x: screenWidth / 2.0, y: imageHeight * scale / 2.0 + imageOffsetY)
        
        imageView.transform = CGAffineTransform.init(scaleX: scale, y: scale)
        
        let controlViewY = imageOffsetY + imageHeight * scale
        let controlViewHeight = screenHeight - controlViewY
        
        controlView.frame = CGRect(x: 0, y: controlViewY, width: screenWidth, height: controlViewHeight)
        let mat = Mat(rows: Int32(image?.size.width ?? 0), cols: Int32(image?.size.width ?? 0), type: CvType.CV_8UC4, scalar: Scalar(0, 0, 0, 0))
        maskImageView.image = mat.toUIImage()
        maskImageView.frame = imageView.bounds
    }
    /// 点击图片 进行图像分割
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let point = touch.location(in: self.imageView)
            guard self.imageView.layer.contains(point) else { return }
            
            currentPoint = point
            
            runModel()
        }
    }
    /// shape值变化
    @objc func shapeSliderChange(_ sender: UISlider) {
        let width = Int32(sender.value)
        
        paddlePredictor.config.updateInputShapeWidth(width, height: width)
        shapeView?.updateShapeLabel(sender.value)
        
        runModel()
    }
    /// 阈值变化
    @objc func thresholdSliderChange(_ sender: UISlider) {
        
        paddlePredictor.config.threshold = sender.value
        thresholdView?.updateThresholdLabel(sender.value)
        
        guard currentPoint != CGPoint.zero else { return }
        paddlePredictor.fetchPredictImage(byThreshold: sender.value) { outputImage in
            self.maskImageView.image = outputImage
        }
        self.updateTimeConsume()
    }
    
    /// cpu数量调整
    @objc func cpuNumSliderChange(_ sender: UISlider) {
        paddlePredictor.config.cpuThreadNum = Int32(sender.value)
    }
    
        
    @objc func modelSelectClick() {
        let sheetVC = UIAlertController(title: "选择模型", message: "", preferredStyle: .actionSheet)
        let modelList = ["通用高精度", "通用轻量化", "人像高精度", "人像轻量化"]
        for modelName in modelList {
            let action = UIAlertAction(title: modelName, style: .default) { (paramAction:UIAlertAction!) in
                let modelType = ModelType(UInt(modelList.firstIndex(of: modelName) ?? 0))
                
                self.modelBtn.setTitle(String(format: " 当前模型：%@", modelName), for: .normal)
                
                self.paddlePredictor.config.updateModel(with: modelType)
                self.paddlePredictor.load(modelType)
                
                self.runModel()
            }
            
            sheetVC.addAction(action)
        }
        present(sheetVC, animated: true, completion: nil)
    }
    
    /// 更新耗时显示
    func updateTimeConsume() {
        let processDuration = String(format: "%.2f", paddlePredictor.processDuration * 1000.0)
        let runDuration = String(format: "%.2f", paddlePredictor.runDuration * 1000.0)
        let visualizeDuration = String(format: "%.2f", paddlePredictor.visualizeDuration * 1000.0)
        timeLabelList[0].text = "预处理耗时" + processDuration + "ms"
        timeLabelList[1].text = "模型推理耗时：" + runDuration + "ms"
        timeLabelList[2].text = "输出处理耗时：" + visualizeDuration + "ms"
    }
}

extension PaddleViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    /// 打开相册
    @objc func enterBtnClick(sender: UIButton) {
        imageShow()
    }
    
    // 图库
    func imageShow()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary)
        {
            self.photosPicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            present(photosPicker, animated: true) { }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: false, completion: nil)
        let pickerImage = info[.originalImage]
        image = pickerImage as? UIImage
        resetImage()
        // 更换图片后，更新shape值和阈值
        shapeView?.updateShapeSliderMaxMinValueWithImage(image)
        thresholdView?.updateThresholdValue(0.5)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: false, completion: nil)
    }
    
    class func mask(size: CGSize, rect: CGRect) -> UIImage {
        let mask = Mat(size: Size(width: Int32(size.width), height: Int32(size.height)),
                       type: CvType.CV_8UC4)
        for i in 0..<mask.rows() {
            for j in 0..<mask.cols() {
                do {
                    if Point(x: j, y: i).inside(Rect(x: Int32(rect.origin.x), y: Int32(rect.origin.y), width: Int32(rect.size.width), height: Int32(rect.size.height))) {
                        try mask.put(row: i, col: j, data: [Double]([0, 255, 0, 255]))
                    }
                } catch let error {
                    print("error \(error.localizedDescription)")
                    print("---------------------------")
                }
            }
        }
        return mask.toUIImage()
    }

    class func merge(image: UIImage, mask: UIImage, originAlpha: Double) -> UIImage? {

        guard image.size.width > 0, image.size.height > 0, mask.size.width > 0, mask.size.height > 0 else {
            return nil
        }
        
        let srcMat = Mat(uiImage: image)
        let maskMat = Mat(uiImage: mask)
        let resizeMat = Mat()

        Imgproc.resize(src: maskMat, dst: resizeMat, dsize: srcMat.size(), fx: 0, fy: 0, interpolation: InterpolationFlags.INTER_LINEAR.rawValue)

        let maskDMat = resizeMat.clone()
        switch resizeMat.type() {
        case CvType.CV_8UC1:
            Imgproc.cvtColor(src: resizeMat, dst: maskDMat, code: .COLOR_GRAY2RGBA)
        case CvType.CV_8UC3:
            Imgproc.cvtColor(src: resizeMat, dst: maskDMat, code: .COLOR_RGB2RGBA)
        case CvType.CV_8UC4:
            break
        default:
            return nil
        }

        let srcDMat = srcMat.clone()
        switch srcMat.type() {
        case CvType.CV_8UC1:
            Imgproc.cvtColor(src: srcMat, dst: srcDMat, code: .COLOR_GRAY2RGBA)
        case CvType.CV_8UC3:
            Imgproc.cvtColor(src: srcMat, dst: srcDMat, code: .COLOR_RGB2RGBA)
        case CvType.CV_8UC4:
            break
        default:
            return nil
        }

        let destMat = Mat()
        Core.addWeighted(src1: srcDMat, alpha: originAlpha, src2: maskDMat, beta: 1-originAlpha, gamma: 0.0, dst: destMat)
        return destMat.toUIImage()
    }
}

class SliderView: UIView {
    
    let titleLabel = UILabel(frame:CGRect.zero)
    let slider = UISlider(frame:CGRect.zero)
    let minValue = 100.0
    let maxValue = 500.0
    let minLabel = UILabel(frame: CGRect(x: 0, y: 20, width: 30, height: 30))
    let maxLabel = UILabel(frame: CGRect.zero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    func setupView() {
        // slider
        titleLabel.frame = CGRect(x: 0, y: 0, width: bounds.width, height: 20)
        slider.frame = CGRect(x: 30, y: 20, width: bounds.width - 60, height: 30)
        maxLabel.frame = CGRect(x: bounds.width - 30, y: 20, width: 30, height: 30)
        
        titleLabel.font = UIFont.systemFont(ofSize: 13)
        maxLabel.font = UIFont.systemFont(ofSize: 13)
        minLabel.font = UIFont.systemFont(ofSize: 13)
        
        self.addSubview(titleLabel)
        self.addSubview(slider)
        self.addSubview(maxLabel)
        self.addSubview(minLabel)
    }
    
    /// 设置shape值的slider
    func updateShapeSliderMaxMinValueWithImage(_ inputImage:UIImage?) {
        guard let image = inputImage else { return }
        
        let imageMinSide = min(image.size.width, image.size.height)
        let maxWidth = Float(min(imageMinSide, maxValue))
        let minWidth = Float(min(maxWidth, 100))
        let currentValue = (maxWidth + minWidth) / 2.0
        
        slider.maximumValue = maxWidth
        slider.minimumValue = minWidth
        slider.value = currentValue
        
        updateShapeDisplay(minWidth, maxWidth: maxWidth, value: currentValue)
        updateShapeLabel(currentValue)
    }
    /// 更新默认阈值
    func updateThresholdValue(_ value: Float) {
        slider.value = value
        updateThresholdLabel(value)
    }
    
    /// 更新shape值的显示
    func updateShapeLabel(_ value: Float) {
        titleLabel.text = "shape值：" + String(Int(value))
    }
    
    /// 更新threshold值的显示
    func updateThresholdLabel(_ value: Float) {
        titleLabel.text = "阈值：" + String(format: "%.2f", value)
    }
    
    /// 更新最大最小值显示
    func updateShapeDisplay(_ minWidth: Float, maxWidth: Float, value: Float) {
        minLabel.text = String(Int(minWidth))
        maxLabel.text = String(Int(maxWidth))
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

