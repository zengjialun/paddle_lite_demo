//
//  PaddleCUtils.m
//  PaddleDemo
//
//  Created by 曾嘉伦 on 2021/12/8.
//

#import "PaddleCUtils.h"

float *input_image_data;
float *input_coord_data;

float *output_data;

// 取缓存、处理预测输出数据时，如果尺寸不对会崩溃
int currentInputWidth;
int currentInputHeight;

int outputWidth;
int outputHeight;

@implementation PaddleCUtils

// MARK: - 输入

int getIndex(int x, int y, PaddleConfig * config) {
    return y * config.coordShape.width + x;
}

/// 将坐标点，转换为对应的像素集合(返回数组对应单通道张量)
int * getCoordIndexList(int clickX, int clickY, PaddleConfig * config, int imageWidth, int imageHeight) {
    
    CGFloat x = clickX * config.coordShape.width / imageWidth;
    CGFloat y = clickY * config.coordShape.height / imageHeight;
    // 防止index越界
    int minX = 5;
    int minY = 5;
    int maxX = config.coordShape.width - 5;
    int maxY = config.coordShape.height - 5;
    if (x < minX) { x = minX; }
    if (y < minY) { y = minY; }
    if (x > maxX) { x = maxX; }
    if (y > maxY) { y = maxY; }
    // 根据坐标 扩大像素标记范围
    int *indexList = new int[81];
    int* list = indexList;
    
    *(list++) = getIndex(x, y - 5, config);
    
    for (int i = 0; i <= 6; i++) {
        *(list++) = getIndex(x - 3 + i, y - 4, config);
    }
    for (int i = 0; i <= 8; i++) {
        *(list++) = getIndex(x - 4 + i, y - 3, config);
    }
    for (int i = 0; i <= 8; i++) {
        *(list++) = getIndex(x - 4 + i, y - 2, config);
    }
    for (int i = 0; i <= 8; i++) {
        *(list++) = getIndex(x - 4 + i, y - 1, config);
    }
    for (int i = 0; i <= 10; i++) {
        *(list++) = getIndex(x - 5 + i, y, config);
    }
    for (int i = 0; i <= 8; i++) {
        *(list++) = getIndex(x - 4 + i, y - 1, config);
    }
    for (int i = 0; i <= 8; i++) {
        *(list++) = getIndex(x - 4 + i, y - 2, config);
    }
    for (int i = 0; i <= 8; i++) {
        *(list++) = getIndex(x - 4 + i, y - 3, config);
    }
    for (int i = 0; i <= 6; i++) {
        *(list++) = getIndex(x - 3 + i, y - 4, config);
    }
    *(list++) = getIndex(x, y + 5, config);
    
    return indexList;
}

// 图片数据前期准备（加缓存）
void image_preprocess(float* dstData, UIImage * srcImage, int input_width, int input_height, bool updateCache) {
        
    // 不需要更新缓存 则直接取
    if (!updateCache) {
        bool success = fillin_preprocess_cache(dstData, InPutTypeImage, input_width, input_height);
        if (success) { return; }
    }
    
    // 无缓存则或需要更新缓存，则进行预处理操作
    std::vector<float> mean = {0.0f, 0.0f, 0.0f};
    std::vector<float> scale = {1.0f, 1.0f, 1.0f};
    image_preprocess(dstData, srcImage, input_width, input_height, mean, scale, YES);
    // 更新缓存
    cache_preprocess(dstData, InPutTypeImage, input_width, input_height);
    // 记录当前处理数据的尺寸
    currentInputWidth = input_width;
    currentInputHeight = input_height;
}

// 图片数据前期准备
void image_preprocess(float* dstData, UIImage * srcImage, int input_width, int input_height,
                            std::vector<float> mean, std::vector<float> scale, bool is_scale) {
    Mat *img_in = [[Mat alloc]initWithUIImage: srcImage];
    
    switch (img_in.type) {
        case CV_8UC1:
                [Imgproc cvtColor: img_in dst: img_in code: COLOR_GRAY2RGB];
                break;
                
        case CV_8UC4:
                [Imgproc cvtColor: img_in dst: img_in code: COLOR_RGBA2RGB];
                break;
    }
    
    [Imgproc resize: img_in dst: img_in dsize: [Size2i width:input_width height:input_height] fx: 0 fy: 0];
    
    float scale_factor = is_scale? 1 / 255.f : 1.f;
    [img_in convertTo: img_in rtype: CV_32FC3 alpha: scale_factor];
    
    const float* srcImg = reinterpret_cast<const float*>(img_in.nativeRef.data);
    
    neon_mean_scale(srcImg, dstData, input_width * input_height, mean, scale);
}

/// neon算法加速
void neon_mean_scale(const float* srcImg, float* dstData, int size,
                     std::vector<float> mean, std::vector<float> scale) {
    // 一个float32x4_t的NEON寄存器，它具有4个lane（通道），每个lane（通道）有一个float32的值
    float32x4_t vmean0 = vdupq_n_f32(mean[0]);// 初始化存入四个float32类型的值：mean[0]
    float32x4_t vmean1 = vdupq_n_f32(mean[1]);// vdupq_n_f32(0.0f) => {0.0, 0.0, 0.0, 0.0}
    float32x4_t vmean2 = vdupq_n_f32(mean[2]);//
    float32x4_t vscale0 = vdupq_n_f32(1.f / scale[0]);// 初始化存入四个float32类型的值：1.f / scale[0]
    float32x4_t vscale1 = vdupq_n_f32(1.f / scale[1]);//
    float32x4_t vscale2 = vdupq_n_f32(1.f / scale[2]);//
    
    float* dout_c0 = dstData;// R通道首地址
    float* dout_c1 = dstData + size;// G通道首地址 = R首地址平移size
    float* dout_c2 = dstData + size * 2;// B通道首地址 = R首地址平移2 * size
    
    int i = 0;
    for (; i < size - 3; i += 4) {
        // float32x4x3_t <=> float32x4_t [3]
        /*  vld3q_f32：间隔，交叉存取，
            间隔为3
            读取12个float32进3个NEON寄存器中。
            3个寄存器的值分别为：{ptr[0],ptr[3],ptr[6],ptr[9]}，
                             {ptr[1],ptr[4],ptr[7],ptr[10]}，
                             {ptr[2],ptr[5],ptr[8],ptr[11]}。
         */
        float32x4x3_t vin3 = vld3q_f32(srcImg);
        
        // https://blog.csdn.net/fuwenyan/article/details/78793907
        float32x4_t vsub0 = vsubq_f32(vin3.val[0], vmean0);// vsubq_f32：浮点减法 vin3.val[0] - vmean0
        float32x4_t vsub1 = vsubq_f32(vin3.val[1], vmean1);
        float32x4_t vsub2 = vsubq_f32(vin3.val[2], vmean2);
        float32x4_t vs0 = vmulq_f32(vsub0, vscale0);// vmulq_f32：浮点数乘法
        float32x4_t vs1 = vmulq_f32(vsub1, vscale1);
        float32x4_t vs2 = vmulq_f32(vsub2, vscale2);
        vst1q_f32(dout_c0, vs0);// vst1q_f32：单通道赋值，把vs0赋值给dout_c0
        vst1q_f32(dout_c1, vs1);
        vst1q_f32(dout_c2, vs2);
        
        // 指针平移
        srcImg += 12;
        dout_c0 += 4;
        dout_c1 += 4;
        dout_c2 += 4;
    }
    for (; i < size; i++) {
        *(dout_c0++) = (*(srcImg++) - mean[0]) / scale[0];
        *(dout_c1++) = (*(srcImg++) - mean[1]) / scale[1];
        *(dout_c2++) = (*(srcImg++) - mean[2]) / scale[2];
    }
}

/// 坐标点 前期准备
///
void coord_preprocess(float* dstData, int clickX, int clickY, PaddleConfig* config, int imageWidth, int imageHeight) {
    
    int width = config.coordShape.width;
    int height = config.coordShape.height;
    
    int size = width * height;
    
    float* dout_c0 = dstData;
    float* dout_c1 = dstData + size;
    float* dout_c2 = dstData + size * 2;
    
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++){
            *(dout_c0++) = 0;
            *(dout_c1++) = 0;
            *(dout_c2++) = 0;
        }
    }
    
    // 为G通道中的像素点赋值
    float* dout_G = dstData + size;
    // 获取坐标对应的像素点集合
    int *indexList = getCoordIndexList(clickX, clickY, config, imageWidth, imageHeight);
    for (int i = 0; i < 81; i++) {
        int index = indexList[i];
        dout_G[index] = 1;
    }
}

// MARK: 输入缓存

void cache_preprocess(float* src_data, InPutType type, int input_width, int input_height) {
    int data_size = input_width * input_height * 3 * 4;
    if (src_data) {
        if (type == InPutTypeImage) {
            delete_c_array(&input_image_data);
            input_image_data = (float*)malloc(data_size);
            memcpy(input_image_data, src_data, data_size);
        }else {
            delete_c_array(&input_coord_data);
            input_coord_data = (float*)malloc(data_size);
            memcpy(input_coord_data, src_data, data_size);
        }
    }
}

bool fillin_preprocess_cache(float* inputData, InPutType type, int input_width, int input_height) {
    if (currentInputWidth != input_width || currentInputHeight != input_height || inputData == NULL) { return false; }
    
    if (type == InPutTypeImage) {
        if (input_image_data == NULL) { return false; }
        memcpy(inputData, input_image_data, input_width * input_height * 3 * 4);
    }else {
        if (input_coord_data == NULL) { return false; }
        memcpy(inputData, input_coord_data, input_width * input_height * 3 * 4);
    }
    return true;
}

bool fillin_preprocess_cache(float* srcData, float* desData, int input_width, int input_height) {
    if (currentInputWidth != input_width || currentInputHeight != input_height || srcData == NULL || desData == NULL) { return false; }
    
    memcpy(desData, srcData, input_width * input_height * 3  * 4);
    
    return true;
}

void delete_preprocess_cache() {
    delete_c_array(&input_image_data);
    delete_c_array(&input_coord_data);
    delete_c_array(&output_data);
}

void delete_c_array(float** data) {
    if (*data == NULL) { return; }
    delete [] *data;
    *data = NULL;
}

// MARK: - 输出
int getOutputWidth() {
    return outputWidth;
}
int getOutputHeight() {
    return outputHeight;
}

void cache_output_data(std::unique_ptr<const paddle::lite_api::Tensor> output_tensor) {
    
    float * output = output_tensor->mutable_data<float>();
    auto shape_out = output_tensor->shape();
    
    if (output == NULL) { return; }
    
    // 计算输出长度
    long outputSize = 1;
    int count = 0;
    for (auto& i : shape_out) {
        outputSize *= i;
        if (count == 2) {
            outputHeight = int(i);
        }else if (count == 3) {
            outputWidth = int(i);
        }
        count++;
    }
    long data_size = outputSize * 4;
    
    delete_c_array(&output_data);
    output_data = (float*)malloc(data_size);
    
    memcpy(output_data, output, data_size);
    
}

/// 图像分割后期处理
void Postprocess(uint8_t** outPutData, float threshold) {
    // 定义两个颜色值
    uint8_t colorsMap[2] = {0, 255};
    // 输出数据
    auto output = output_data;
    // 计算输出长度
    long outputSize = outputHeight * outputWidth;
    
    // 初始化单通道位图数据空间
    uint8_t *bitMapData = (uint8_t*)malloc(outputSize);
    // 默认阈值为0.5,对应的对比值为0
    float comparedValue = (threshold - 0.5) * 7.5;
    int colorIndex = 0;
    for (int i = 0; i < outputSize; i++) {
        if (__builtin_expect(!!(output[i] > comparedValue), 0)) {
            colorIndex = 1;
        }else {
            colorIndex = 0;
        }
        // 根据输出构造位图数据 将颜色值写入对应位置
        bitMapData[i] = colorsMap[colorIndex];
    }
    
    *outPutData = bitMapData;
}


//void neon_memcpy(volatile unsigned char *dst, volatile unsigned char *src, int sz) {
//    if (sz & 63)
//        sz = (sz & -64) + 64;
//    asm volatile (
//    "NEONCopyPLD: \n"
//            " PLD  %[src],#0xC0 \n"
//            " VLDM %[src]!,{d0-d7} \n"
//            " VSTM %[dst]!,{d0-d7} \n"
//            " SUBS %[sz],%[sz],#0x40 \n"
//            " BGT NEONCopyPLD \n"
//    : [dst]"+r"(dst), [src]"+r"(src), [sz]"+r"(sz) : : "d0", "d1", "d2", "d3", "d4", "d5", "d6", "d7", "cc", "memory");
//}

@end
