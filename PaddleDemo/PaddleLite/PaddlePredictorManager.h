//
//  PaddlePredictorManager.h
//  PaddleDemo
//
//  Created by 曾嘉伦 on 2021/12/8.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PaddleConfig.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^AdjustThresholdCallBack)(UIImage * maskImg);
typedef void(^BitmapDataCallBack)(NSData * bitmapData, int shapeWidth, int shapeHeight);
typedef void(^BitmapCallBack)(uint8_t * bitmapData, int shapeWidth, int shapeHeight);
typedef void(^RunModelCallBack)();

@interface PaddlePredictorManager : NSObject

@property (nonatomic, strong) PaddleConfig * config;

@property (nonatomic, readonly, assign) CFTimeInterval runDuration;
@property (nonatomic, readonly, assign) CFTimeInterval processDuration;
@property (nonatomic, readonly, assign) CFTimeInterval visualizeDuration;

/// 开始预测
/// @param inputImage 图片
/// @param point 点击位置
/// @param callBack 成功回调 不附带数据
- (void)runModel:(nullable UIImage *)inputImage point:(CGPoint)point callBack:(RunModelCallBack)callBack;


// MARK: 获取阈值对应预测结果

/// 获取图片
/// @param threshold 阈值
/// @param callBack  返回单通道的图片 maskImage
- (void)fetchPredictImageByThreshold:(float)threshold callBack:(AdjustThresholdCallBack)callBack;

/// 获取NSData数组
/// @param threshold 阈值
/// @param callBack  返回单通道的图片 NSData数组
- (void)fetchPredictBitmapDataByThreshold:(float)threshold callBack:(BitmapDataCallBack)callBack;

/// 获取位图Uint8数组指针
/// @param threshold 阈值
/// @param callBack  返回单通道的图片 位图Uint8数组指针
- (void)fetchPredictBitmapByThreshold:(float)threshold callBack:(BitmapCallBack)callBack;

- (void)loadModel:(ModelType)type;

@end

@interface WBRWLock: NSObject
 
- (void)rLock;
- (void)rUnlock;
- (void)wLock;
- (void)wUnlock;

@end

NS_ASSUME_NONNULL_END
