//
//  PaddleCUtils.h
//  PaddleDemo
//
//  Created by 曾嘉伦 on 2021/12/8.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#include <arm_neon.h>
#include <vector>
#include <string>
#import <opencv2/opencv2.h>
#include "paddle_api.h"
#import "PaddleConfig.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    InPutTypeImage,
    InPutTypeCoord,
} InPutType;

@interface PaddleCUtils : NSObject

// MARK: 输入
/// 预处理图片（带缓存）
/// @param dstData 输出数据的指针
/// @param srcImg 输入图片
/// @param width shape宽
/// @param height shape高
/// @param updateCache 是否更新缓存
void image_preprocess(float* dstData, UIImage * srcImg, int width, int height, bool updateCache);

/// 预处理点
/// @param dstData 输出数据的指针
/// @param clickX 点击坐标x值
/// @param clickY 点击坐标y值
/// @param config PaddleConfig
/// @param imageWidth 原图宽
/// @param imageHeight 原图高
void coord_preprocess(float* dstData, int clickX, int clickY, PaddleConfig* config, int imageWidth, int imageHeight);


// MARK:  输出
/// 处理输出数据
/// @param outPutData 输出bitmap数组指针
/// @param threshold 阈值
void Postprocess(uint8_t** outPutData, float threshold);


/// 存储原始的输出结果
/// @param output_tensor 预测输出
void cache_output_data(std::unique_ptr<const paddle::lite_api::Tensor> output_tensor);


bool fillin_preprocess_cache(float* srcData, float* desData, int input_width, int input_height);

// 获取当前预测输出的宽高
int getOutputWidth();
int getOutputHeight();

/// 释放缓存
void delete_preprocess_cache();

@end

NS_ASSUME_NONNULL_END
