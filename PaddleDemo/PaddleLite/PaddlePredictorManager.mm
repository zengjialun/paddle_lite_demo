//
//  PaddlePredictorManager.m
//  PaddleDemo
//
//  Created by 曾嘉伦 on 2021/12/8.
//

#import "PaddlePredictorManager.h"
#include <iostream>
#include <mutex>
#include "paddle_use_ops.h"
#include "paddle_use_kernels.h"
#import "PaddleCUtils.h"
#import <pthread/pthread.h>

typedef void(^CalculationTime)();
std::shared_ptr<paddle::lite_api::PaddlePredictor> predictor;

@interface PaddlePredictorManager ()

@property (nonatomic, assign) CFTimeInterval runDuration;
@property (nonatomic, assign) CFTimeInterval processDuration;
@property (nonatomic, assign) CFTimeInterval visualizeDuration;
@property (nonatomic, strong) WBRWLock * lock;

// 用于优化 暂时未用
@property (nonatomic) std::vector<float> scale;
@property (nonatomic) std::vector<float> mean;
// 记录预测参数
@property (nonatomic, strong) UIImage * oldImage;
@property (nonatomic, assign) int oldInputWidth;
@property (nonatomic, assign) CGPoint oldPoint;

@property (nonatomic, strong) dispatch_semaphore_t semaphore;
@property (nonatomic, strong) dispatch_queue_t queue;
@property (nonatomic, strong) dispatch_queue_t outputQueue;

@end

@implementation PaddlePredictorManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.config = [[PaddleConfig alloc] init];
        self.mean = {0.0f, 0.0f, 0.0f};
        self.scale = {1.0f, 1.0f, 1.0f};
        
        self.semaphore = dispatch_semaphore_create(0);
        self.queue = dispatch_queue_create("paddle_serial_queue",DISPATCH_QUEUE_SERIAL);
        self.outputQueue = dispatch_queue_create("paddle_serial_output_queue",DISPATCH_QUEUE_SERIAL);
        
        [self loadModel: LOW_COMMON];
    }
    return self;
}

// 正在预测时 不可调用
- (void)loadModel:(ModelType)type {
    dispatch_async(self.queue, ^{
        predictor = nil;
        [self.config updateModelWithType: type];
        // 模型地址
        NSString *path = self.config.modelPath;
        std::string paddle_mobilenetv1_dir = std::string([path UTF8String]);
        // 耗能模式
        paddle::lite_api::PowerMode powerMode = paddle::lite_api::LITE_POWER_HIGH;
        switch (self.config.cpuPowerMode) {
            case LITE_POWER_HIGH:
                powerMode = paddle::lite_api::LITE_POWER_HIGH;
                break;
            case LITE_POWER_LOW:
                powerMode = paddle::lite_api::LITE_POWER_LOW;
                break;
            case LITE_POWER_FULL:
                powerMode = paddle::lite_api::LITE_POWER_FULL;
                break;
            case LITE_POWER_NO_BIND:
                powerMode = paddle::lite_api::LITE_POWER_NO_BIND;
                break;
            case LITE_POWER_RAND_HIGH:
                powerMode = paddle::lite_api::LITE_POWER_RAND_HIGH;
                break;
            case LITE_POWER_RAND_LOW:
                powerMode = paddle::lite_api::LITE_POWER_RAND_LOW;
                break;
                
            default:
                break;
        }
        // 创建预测类
        paddle::lite_api::MobileConfig mConfig;
        mConfig.set_power_mode(powerMode);
        mConfig.set_model_from_file(paddle_mobilenetv1_dir);
        if (self.config.modelType == LOW_COMMON_METAL) {
            std::string metal_path = std::string([self.config.metalPath UTF8String]);
            mConfig.set_metal_lib_path(metal_path);
            mConfig.set_metal_use_mps(true);
        }
        predictor = CreatePaddlePredictor<paddle::lite_api::MobileConfig>(mConfig);
        
        dispatch_semaphore_signal(self.semaphore);
    });
}

- (void)runModel:(UIImage *)inputImage point:(CGPoint)point callBack:(RunModelCallBack)callBack {
    if (inputImage == nil) { return ; }
    
//    [self.config updateInputShapeWidth:inputImage.size.width height:inputImage.size.height];
    PaddleConfig * config = [self.config copy];
    int input_width = self.config.inputShape.width;
    int input_height = self.config.inputShape.height;
    float imageWidth = inputImage.size.width;
    float imageHeight = inputImage.size.height;
    
    dispatch_async(self.queue, ^{
        @synchronized (self) {
            // 判断参数的改变
            int oldWidth = self.oldInputWidth;
            bool shapeChange = oldWidth != input_width;
            bool imageChange = self.oldImage != inputImage;
            bool updateImageCache = (shapeChange || imageChange);
            
            // 输入预处理
            self.processDuration = [self calculationTime:^{
                int data_size = input_width * input_height * 3 * 4;
                float* imageData = (float*)malloc(data_size);
                float* coordData = (float*)malloc(data_size);
                image_preprocess(imageData, inputImage, input_width, input_height, updateImageCache);
                coord_preprocess(coordData, point.x, point.y, config, imageWidth, imageHeight);
                
                if (predictor == nullptr) {
                    dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_FOREVER);
                }
                
                // 获取第一个输入 image
                std::unique_ptr<paddle::lite_api::Tensor> image_tensor(predictor->GetInput(0));
                // 获取第二个输入 coord_features
                std::unique_ptr<paddle::lite_api::Tensor> coord_tensor(predictor->GetInput(1));
                
                // 重置size
                image_tensor->Resize({1, 3, input_height, input_width});
                coord_tensor->Resize({1, 3, input_height, input_width});
                
                // 将数据填充到预测输入
                fillin_preprocess_cache(imageData, image_tensor->mutable_data<float>(), input_width, input_height);
                fillin_preprocess_cache(coordData, coord_tensor->mutable_data<float>(), input_width, input_height);
                
                delete [] imageData;
                delete [] coordData;
            }];
            
            // 开始运行预测
            self.runDuration = [self calculationTime:^{
                predictor->Run();
            }];
            
            dispatch_barrier_async(self.outputQueue, ^{
                // 缓存输出
                NSLog(@"写-开始+++++");
                cache_output_data(predictor->GetOutput(0));
                NSLog(@"写-结束+++++");
                
                // 预测完成
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (callBack) {
                        callBack();
                    }
                });
            });
            
            // 记录预测参数 用于下一次判断是否更新缓存
            self.oldImage = inputImage;
            self.oldPoint = point;
            self.oldInputWidth = input_width;
        }
    });
}

-(CFTimeInterval)calculationTime:(CalculationTime)block {
    CFTimeInterval startTime = CACurrentMediaTime();
    if (block) {
        block();
    }
    return CACurrentMediaTime() - startTime;
}

/// 阈值调整
- (void)fetchPredictImageByThreshold:(float)threshold callBack:(AdjustThresholdCallBack)callBack{
    self.config.threshold = threshold;
    if (self.oldImage == nil) { return ; }
    
    CGSize orgSize = self.oldImage.size;
    __weak typeof(self) weakSelf = self;
    dispatch_barrier_async(self.outputQueue, ^{
        NSLog(@"读-开始-----");
        // 处理输出数据
        __block UIImage * maskImage;
        self.visualizeDuration = [self calculationTime:^{
            uint8_t * outPutData = nil;
            [weakSelf.lock rLock];
            Postprocess(&outPutData, threshold);
            [weakSelf.lock rUnlock];

            int height = getOutputHeight();
            int width = getOutputWidth();
            
            NSData * data = [[NSData alloc] initWithBytes: outPutData length: width * height];
            Mat * outMat = [[Mat alloc] initWithRows:height  cols: width type: CV_8UC1 data: data];
            
            delete [] outPutData;
            outPutData = NULL;
//                {
            //不寻找最大区域
//                // 重置成原图尺寸的图片
//                Size2i * originalSize = [[Size2i alloc] initWithWidth: orgSize.width height: orgSize.height];
//                [Imgproc resize: outMat dst: outMat dsize: originalSize];
//
//                maskImage = outMat.toUIImage;
//            }
            // 寻找最大区域
            NSMutableArray<NSMutableArray<Point2i *> *> * contours = [[NSMutableArray alloc]init];
            Mat * hierarchy = [[Mat alloc]init];
            [Imgproc findContours:outMat contours:contours hierarchy:hierarchy mode:RETR_EXTERNAL method:CHAIN_APPROX_SIMPLE];
            
            double maxArea = 0;
            int maxIndex = -1;
            for (int i = 0; i < contours.count; i++) {
                NSMutableArray<Point2i *> * contour = contours[i];
                MatOfPoint2i *pointMat = [[MatOfPoint2i alloc]initWithArray:contour];
                double area = [Imgproc contourArea: pointMat];
                if (area > maxArea) {
                    maxIndex = i;
                    maxArea = area;
                }
            }
            if (maxIndex != -1) {
                Mat * mat = [[Mat alloc]initWithRows:outMat.rows cols:outMat.cols type:CV_8UC4 scalar:[[Scalar alloc]initWithV0:0 v1:0 v2:0 v3:0]];
                NSArray<NSArray<Point2i *> *> * points = [NSArray arrayWithObject:contours[maxIndex]];
                
                [Imgproc fillPoly:mat pts:points color:[[Scalar alloc]initWithV0:255 v1:255 v2:0 v3:128]];
                
                [Imgproc resize:mat dst:mat dsize:[[Size2i alloc]initWithWidth:orgSize.width height:orgSize.height]];
                
                maskImage = mat.toUIImage;
            } else{
                maskImage = [[Mat alloc]initWithRows:orgSize.width cols:orgSize.height type:CV_8UC4 scalar:[[Scalar alloc]initWithV0:0 v1:0 v2:0 v3:0]].toUIImage;
            }
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (callBack) {
                callBack(maskImage);
            }
        });
        NSLog(@"读-结束-----");
    });
}

/// 阈值调整
- (void)fetchPredictBitmapDataByThreshold:(float)threshold callBack:(BitmapDataCallBack)callBack{
    self.config.threshold = threshold;
    if (self.oldImage == nil) { return ; }
        
    dispatch_async(self.outputQueue, ^{

        int height = getOutputHeight();
        int width = getOutputWidth();
        // 处理输出数据
        __block NSData * maskData;
        self.visualizeDuration = [self calculationTime:^{
            uint8_t * outPutData = nil;
            Postprocess(&outPutData, threshold);
            
            NSData * data = [[NSData alloc] initWithBytes: outPutData length: width * height];
            maskData = data;
            
            delete [] outPutData;
            outPutData = NULL;
        }];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (callBack) {
                callBack(maskData, width, height);
            }
        });
    });
}

/// 阈值调整
- (void)fetchPredictBitmapByThreshold:(float)threshold callBack:(BitmapCallBack)callBack{
    self.config.threshold = threshold;
    if (self.oldImage == nil) { return ; }
        
    dispatch_async(self.outputQueue, ^{
        // 处理输出数据
        __block uint8_t * maskData;
        self.visualizeDuration = [self calculationTime:^{
            uint8_t * outPutData = NULL;
            Postprocess(&outPutData, threshold);
            maskData = outPutData;
            outPutData = NULL;
        }];
        
        int height = getOutputHeight();
        int width = getOutputWidth();
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (callBack) {
                callBack(maskData, width, height);
            }
            delete [] maskData;
            maskData = NULL;
        });
    });
}

-(void)dealloc{
    delete_preprocess_cache();
    predictor = nil;
}

@end


