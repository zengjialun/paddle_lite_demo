//
//  PaddleConfig.h
//  PaddleDemo
//
//  Created by 曾嘉伦 on 2021/12/8.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// tensor格式
typedef struct {
    int num;// 个数
    int channels; // 通道数
    int height;
    int width;
}PaddleShape;

NS_ASSUME_NONNULL_BEGIN

// 模型类型
typedef enum : NSUInteger {
    HIGH_COMMON,
    LOW_COMMON,
    LOW_COMMON_METAL,
    HIGH_FACE,
    LOW_FACE,
} ModelType;

typedef enum : NSUInteger {
    // 绑定大核运行模式。如果 ARM CPU 支持 big.LITTLE，则优先使用并绑定 Big cluster，如果设置的线程数大于大核数量，则会将线程数自动缩放到大核数量。如果系统不存在大核或者在一些手机的低电量情况下会出现绑核失败，如果失败则进入不绑核模式。
    LITE_POWER_HIGH,
    // 绑定小核运行模式。如果 ARM CPU 支持 big.LITTLE，则优先使用并绑定 Little cluster，如果设置的线程数大于小核数量，则会将线程数自动缩放到小核数量。如果找不到小核，则自动进入不绑核模式。
    LITE_POWER_LOW,
    // 大小核混用模式。线程数可以大于大核数量，当线程数大于核心数量时，则会自动将线程数缩放到核心数量。
    LITE_POWER_FULL,
    // 不绑核运行模式（推荐）。系统根据负载自动调度任务到空闲的 CPU 核心上。
    LITE_POWER_NO_BIND,
    // 轮流绑定大核模式。如果 Big cluster 有多个核心，则每预测 10 次后切换绑定到下一个核心。
    LITE_POWER_RAND_HIGH,
    // 轮流绑定小核模式。如果 Little cluster 有多个核心，则每预测 10 次后切换绑定到下一个核心。

    LITE_POWER_RAND_LOW
} CpuPowerMode;

@interface PaddleConfig : NSObject <NSCopying>

@property (nonatomic, readonly, assign) ModelType modelType;// 模型类型
@property (nonatomic, readonly, strong) NSString * modelPath;// 模型地址
@property (nonatomic, readonly, strong) NSString * modelName;
@property (nonatomic, readonly, strong) NSString * metalPath;// metal库地址
@property (nonatomic, readonly, assign) PaddleShape inputShape;// 输入图片 tensor格式
@property (nonatomic, readonly, assign) PaddleShape coordShape;// 输入点 tensor格式（与inputShape一致）
@property (nonatomic, assign) int cpuThreadNum;// 工作线程数 （暂时无效）
@property (nonatomic, assign) CpuPowerMode cpuPowerMode;// 能耗模式
@property (nonatomic, assign) float threshold;// 阈值

/// 更改ShapeWidth
- (void)updateInputShapeWidth:(int)width height:(int)height;

/// 更新配置中的模型
- (void)updateModelWithType:(ModelType)type;

/// 获取模型名称
- (NSString *)getModelNameWithType:(ModelType)type;

@end

NS_ASSUME_NONNULL_END
