//
//  AppDelegate.swift
//  PaddleDemo
//
//  Created by 曾嘉伦 on 2021/12/6.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow.init(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        let nav = UINavigationController(rootViewController: ViewController())
        window?.rootViewController = nav
        // Override point for customization after application launch.
        return true
    }

}

